define([
    'backbone'
], function (
    Backbone
) {
    'use strict';
    
	var UserModel = Backbone.Model.extend({
		defaults: {
			'name': 'No Name'
		}
	});

	return UserModel;
});
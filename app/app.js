define([
	'collections/Users',
	'views/UserListView',
    'jquery'
], function (
	UserCollection,
	UserView,
    $
) {
    'use strict';
    
	var initialize = function () {
	    
	    var data = [{'name':'henry'}, {'name':'eddy'}, {'name':'hao'}, {'name':'megan'}, {'name':'healy'}],
	    	collection = new UserCollection(data),
	    	view = new UserView({ collection: collection });
        
        view.on('users:get', function () {
            loadUsers().done(function () {
                view.trigger('users:loaded');    
            });
        });

	    view.render();
	},
        loadUsers = function () {
            var deferred = $.Deferred();
            
            setTimeout(function () {
                deferred.resolve();
            }, 2000);
            
            return deferred.promise();
        };

	return { 'initialize' : initialize };
});

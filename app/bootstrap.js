requirejs.config({
    paths: {
        'jquery': '../libs/jquery',
        'jquery-private': '../libs/jquery-private',
        'underscore': '../libs/underscore',
        'twbootstrap': '../libs/bootstrap',
        'backbone': '../libs/backbone',
        'text': '../libs/text'
    },
    map: {
        '*': { 'jquery': 'jquery-private' },
        'jquery-private': { 'jquery': 'jquery' }
    },
    shim: {
        'twbootstrap': {
            deps: ['jquery']
        }
    }
});

require(['app'], function (App) {
    'use strict';
    
	App.initialize();
});
define([
	'backbone',
	'models/User',
], function (
    Backbone,
    UserModel
) {
    'use strict';
    
	var UserCollection = Backbone.Collection.extend({
		model: UserModel
	});

	return UserCollection;
});
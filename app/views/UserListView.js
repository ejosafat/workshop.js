define([
	'backbone',
	'underscore',
	'text!templates/userListTemplate.html',
    'twbootstrap'
], function (
	Backbone,
	_,
	userListTemplate
) {
    'use strict';
    
	var UserListView = Backbone.View.extend({
		el: '#page',
        template: _.template(userListTemplate),
        events: {
            'click #loading-example-btn': 'onClickLoading'
        },
        
        initialize: function () {
            this.on('users:loaded', function () {
                this.$('button').button('reset'); 
            });
        },
        
        onClickLoading: function (evt) {
            this.$('button').button('loading');
            this.trigger('users:get');
        },

		render: function () {
			var html = this.template({ users: this.collection.toJSON() });
			this.$el.html(html);
            
            return this;
		}
	});

	return UserListView;
});